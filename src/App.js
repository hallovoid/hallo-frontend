/* global Stripe */

import React, { Component } from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import customTheme from './styles/customTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import LinearProgress from 'material-ui/LinearProgress';
import Formsy from 'formsy-react';
import FormsyText from 'formsy-material-ui/lib/FormsyText';
import RichTextEditor from 'react-rte';
import RichText from './RichText';
import CreditCardFullName from './CreditCardFullName';
import CreditCardNumber from './CreditCardNumber';
import CreditCardExpiryDate from './CreditCardExpiryDate';
import CreditCardCvc from './CreditCardCvc';
import TextField from 'material-ui/TextField';
import CardReactFormContainer from 'card-react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import { blue500, grey400, darkBlack } from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';
import {
  Step,
  Stepper,
  StepLabel,
  StepContent,
} from 'material-ui/Stepper';

import 'whatwg-fetch';

// Needed for onTouchTap
// // http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

function responsiveWidth() {
  let width = window.innerWidth || document.body.clientWidth;
  if (width > 720) {
    return { width: '60%' };
  } else {
    return { width: '95%' };
  }
}

const styles = {
  imageInput: {
    cursor: 'pointer',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
  },
  logoStyle: {
		paddingBottom: 20,
  },
  footerStyle: {
		paddingTop: 20,
    color: '#0371b7'
  },
	paperStyle: {
		margin: 'auto',
		paddingTop: 0,
		paddingRight: 5,
		paddingBottom: 10,
		paddingLeft: 0,
	},
	mainDiv: {
		margin: 'auto',
		padding: 5,
	},
	submitStyle: {
		marginTop: 32,
	},
  rteLabel: {
    marginTop: 20,
    marginBottom: 5,
    fontSize: 16
  },
  previewLinkDivStyle: {
    marginTop: 20,
    marginBottom: 10
  },
  linkStyle: {
    color: '#0371b7',
    textDecoration: 'none',
    wordWrap: 'break-word'
  },
  cardStyle: {
    marginTop: 2,
    marginRight: 2,
    marginLeft: 2,
    marginBottom: 20,
    padding: 10
  },
  cardTitleStyle: {
    padding: 0
  },
  cardTextStyle: {
    padding: 0
  },
  stepContentStyle: {
    paddingLeft: 5,
    paddingRight: 0
  },
  cardWrapper: {
    display: 'inline-block'
  }
};

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: '',
			canSubmit: false,
			canSubmitPayment: false,
      jobDescriptionOnFocus: false,
      jobDescription: RichTextEditor.createEmptyValue(),
      responsibilities: RichTextEditor.createEmptyValue(),
      skills: RichTextEditor.createEmptyValue(),
      educationRequirements: RichTextEditor.createEmptyValue(),
      experienceRequirements: RichTextEditor.createEmptyValue(),
      qualifications: RichTextEditor.createEmptyValue(),
      incentiveCompensation: RichTextEditor.createEmptyValue(),
      jobBenefits: RichTextEditor.createEmptyValue(),
      completed: false,
      generatedPreviewUrl: '',
      generatedJobPostingUrl: '',
      isSubmittingForm: false,
      isGeneratingJobPosting: false,
			finished: false,
			stepIndex: 0,
      submittingPayment: false,
      paymentError: null,
      jobPostingError: false,
      formData: {}
		};


    this._handleImageChange = this._handleImageChange.bind(this);
    this._proceedToPreviewStep = this._proceedToPreviewStep.bind(this);
    this._enableButton = this._enableButton.bind(this);
    this._disableButton = this._disableButton.bind(this);
    this._enablePaymentButton = this._enablePaymentButton.bind(this);
    this._disablePaymentButton = this._disablePaymentButton.bind(this);
    this._notifyFormError = this._notifyFormError.bind(this);
    this._onJobDescriptionChange = this._onJobDescriptionChange.bind(this);
    this._onJobDescriptionFocus = this._onJobDescriptionFocus.bind(this);
    this._onJobDescriptionBlur = this._onJobDescriptionBlur.bind(this);
    this._onResponsibilitiesChange = this._onResponsibilitiesChange.bind(this);
    this._onSkillsChange = this._onSkillsChange.bind(this);
    this._onEducationRequirementsChange = this._onEducationRequirementsChange.bind(this);
    this._onExperienceRequirementsChange = this._onExperienceRequirementsChange.bind(this);
    this._onQualificationsChange = this._onQualificationsChange.bind(this);
    this._onIncentiveCompensationChange = this._onIncentiveCompensationChange.bind(this);
    this._onJobBenefitsChange = this._onJobBenefitsChange.bind(this);
    this._setImagePreview = this._setImagePreview.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
    this.renderStepActions = this.renderStepActions.bind(this);
    this._generatePreview = this._generatePreview.bind(this);
    this._generateJobPosting = this._generateJobPosting.bind(this);
    this._completePayment = this._completePayment.bind(this);
    this._proceedToPaymentStep = this._proceedToPaymentStep.bind(this);

		this.errorMessages = {
			urlError: "Please provide a valid URL (begins with http:// or https://)",
			emailError: "Please provide a valid email",
		}
    Stripe.setPublishableKey(process.env.REACT_APP_STRIPE_PUBLIC_KEY);
  }

  handleNext = () => {
    const {stepIndex} = this.state;
    const nextStep = (stepIndex === 2) ? stepIndex : stepIndex + 1;
    this.setState({
      stepIndex: nextStep
    });
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };

  renderStepActions(step, canSubmit) {
    const {stepIndex, submittingPayment, isGeneratingJobPosting} = this.state;

    return (
      <div style={{margin: '12px 0'}}>
        <RaisedButton
          label={stepIndex === 2 ? 'Finish' : 'Next'}
          disableTouchRipple={true}
          disableFocusRipple={true}
          primary={true}
          type="submit"
          style={{marginRight: 12}}
					disabled={!canSubmit || submittingPayment || isGeneratingJobPosting}
        />
        {step > 0 && (
          <FlatButton
            label="Back"
            disabled={stepIndex === 0 || submittingPayment || isGeneratingJobPosting}
            disableTouchRipple={true}
            disableFocusRipple={true}
            onTouchTap={this.handlePrev}
          />
        )}
      </div>
    );
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    if (!file) {
      this._setImagePreview(null, null);
    }

    reader.onloadend = () => {
      this._setImagePreview(file, reader.result);
    }

    reader.readAsDataURL(file)
  }

  _setImagePreview(file, imgData) {
    this.setState({
      file: file,
      imagePreviewUrl: imgData
    });
  }

  _enableButton() {
    this.setState({
      canSubmit: true,
    });
  }

  _disableButton() {
    this.setState({
      canSubmit: false,
    });
  }

  _enablePaymentButton() {
    this.setState({
      canSubmitPayment: true,
    });
  }

  _disablePaymentButton() {
    this.setState({
      canSubmitPayment: false,
    });
  }

  _proceedToPreviewStep(data) {
    if (this.state.imagePreviewUrl) {
      data.companyLogo = this.state.imagePreviewUrl;
    }
    data.jobDescription = this.state.jobDescription.toString('markdown').trim();
    data.responsibilities = this.state.responsibilities.toString('markdown').trim();
    data.skills = this.state.skills.toString('markdown').trim();
    data.educationRequirements = this.state.educationRequirements.toString('markdown').trim();
    data.experienceRequirements = this.state.experienceRequirements.toString('markdown').trim();
    data.qualifications = this.state.qualifications.toString('markdown').trim();
    data.incentiveCompensation = this.state.incentiveCompensation.toString('markdown').trim();
    data.jobBenefits = this.state.jobBenefits.toString('markdown').trim();
    console.log("submitForm: ", data);
    this.setState({ formData: data });
    this.handleNext();
  }

  _proceedToPaymentStep(event) {
    event.preventDefault();
    this.handleNext();
  }

  _generatePreview() {
    const {formData} = this.state;
    console.log("generatePreview: ", this.state.formData);
    this.setState({ isSubmittingForm: true, completed: false });
    fetch(process.env.REACT_APP_PREVIEW_JOB_API_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData, null, 4)
    }).then(function(res) {
      return res.json();
    }).then((json) => {
      this.setState({ completed: true });
      if (json.success) {
        this.setState({ generatedPreviewUrl: json.url });
      }
    }).then(() => {
      this.setState({ isSubmittingForm: false });
    }).catch(() => {
      this.setState({ isSubmittingForm: false });
    });
  }

  _generateJobPosting(paymentToken) {
    const {stepIndex, formData} = this.state;
    this.setState({ isGeneratingJobPosting: true });
    let theFormData = formData;
    theFormData['paymentToken'] = paymentToken;
    fetch(process.env.REACT_APP_CREATE_JOB_API_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(theFormData, null, 4)
    }).then(function(res) {
      return res.json();
    }).then((json) => {
      if (json.success) {
        this.setState({ generatedJobPostingUrl: json.url });
      } else {
        this.setState({ jobPostingError: true });
      }
    }).then(() => {
      this.setState({
        finished: true,
        stepIndex: stepIndex + 1,
        isGeneratingJobPosting: false
      });
    }).catch(() => {
      this.setState({
        finished: true,
        stepIndex: stepIndex + 1,
        isGeneratingJobPosting: false,
        jobPostingError: true
      });
    });
  }

  _notifyFormError(data) {
    console.error('Form error:', data);
  }

  _onJobDescriptionChange(value) {
    this.setState({ jobDescription: value });
    if (this.props.onChange) {
      // Send the changes up to the parent component as an HTML string.
      // This is here to demonstrate using `.toString()` but in a real app it
      // would be better to avoid generating a string on each change.
//      this.props.onChange(
//        value.toString('html')
//      );
    }
  }

  _onJobDescriptionFocus(event) {
    this.setState({ jobDescriptionOnFocus: true });
  }

  _onJobDescriptionBlur(event) {
    this.setState({ jobDescriptionOnFocus: false });
  }

  _onResponsibilitiesChange(value) {
    this.setState({ responsibilities: value });
  }

  _onSkillsChange(value) {
    this.setState({ skills: value });
  }

  _onEducationRequirementsChange(value) {
    this.setState({ educationRequirements: value });
  }

  _onExperienceRequirementsChange(value) {
    this.setState({ experienceRequirements: value });
  }

  _onQualificationsChange(value) {
    this.setState({ qualifications: value });
  }

  _onIncentiveCompensationChange(value) {
    this.setState({ incentiveCompensation: value });
  }

  _onJobBenefitsChange(value) {
    this.setState({ jobBenefits: value });
  }

  _completePayment(event) {
    const {stepIndex} = this.state;
    event.preventDefault();
    var self = this;
    this.setState({ submittingPayment: true, paymentError: null });
    // send form here
    Stripe.createToken(event.target, function(status, response) {
      if (response.error) {
        console.log("create token failed");
        self.setState({
          submittingPayment: false,
          paymentError: response.error.message
        });
      }
      else {
        console.log("create token successfully");
        // self.setState({ paymentComplete: true, token: response.id });
        // make request to your server here!
        self.setState({
          submittingPayment: false
        });
        self._generateJobPosting(response.id);
      }
    });
  }

  render() {
    const {finished, stepIndex, submittingPayment, paymentError, isGeneratingJobPosting, jobPostingError} = this.state;
    let { urlError, emailError } = this.errorMessages;
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (
        <div>
          <br />
          <br />
          <img alt="company logo preview" src={imagePreviewUrl} width="200px" />
        </div>
      );
    }

    let $linearProgress = null;
    if (this.state.isSubmittingForm) {
      $linearProgress = (<LinearProgress mode="indeterminate" />);
    }

    let $resultPage = null;
    if (this.state.completed) {
      if (this.state.generatedPreviewUrl) {
        $resultPage = (
          <div>
            <div>The preview page link is only valid for 15 minutes.</div>
            <div style={styles.previewLinkDivStyle}>
              <a style={styles.linkStyle} href={this.state.generatedPreviewUrl} target="_blank">Click here to preview your job posting</a>
            </div>
          </div>
        );
      } else {
        $resultPage = (
          <div>Error: failed to generate the preview page..</div>
        );
      }
    }

    let jobDescriptionStyle = {};
    let jobDescriptionOnFocus = this.state.jobDescriptionOnFocus;
    jobDescriptionStyle['color'] = jobDescriptionOnFocus ? blue500 : fade(darkBlack, 0.3);
    let $jobDescriptionLabel = (
      <div style={Object.assign({}, styles.rteLabel, jobDescriptionStyle)}>
        <label>Job description (required)</label>
      </div>
    );

    return (
			<MuiThemeProvider muiTheme={getMuiTheme(customTheme)}>
        <div style={Object.assign({}, styles.mainDiv, responsiveWidth())}>
          <div style={styles.logoStyle}>
            <a href="https://hallo.io">
              <img width="100px" src="./img/logo.png" />
            </a>
          </div>
          <Paper style={Object.assign({}, styles.paperStyle)} zDepth={2}>
            <Stepper activeStep={stepIndex} orientation="vertical">
              <Step>
                <StepLabel>Draft</StepLabel>
                <StepContent style={styles.stepContentStyle}>
                  <Formsy.Form
                    onValid={this._enableButton}
                    onInvalid={this._disableButton}
                    onValidSubmit={this._proceedToPreviewStep}
                    onInvalidSubmit={this._notifyFormError}
                  >
                    <Card style={styles.cardStyle}>
                      <CardTitle
                        style={styles.cardTitleStyle}
                        title="About the job"
                      >
                      </CardTitle>
                      <CardText style={styles.cardTextStyle}>
                        Fill in the details about the job
                      </CardText>
                      <FormsyText
                        name="jobTitle"
                        value={this.state.formData.jobTitle}
                        required
                        floatingLabelText="Job title (required)"
                        multiLine={true}
                      />
                      <br />
                      <FormsyText
                        name="location"
                        value={this.state.formData.location}
                        required
                        floatingLabelText="Location (required)"
                        multiLine={true}
                      />
                      <br />
                      <FormsyText
                        name="salary"
                        value={this.state.formData.salary}
                        floatingLabelText="Salary"
                        multiLine={true}
                      />
                      <br />
                      <FormsyText
                        name="employmentType"
                        value={this.state.formData.employmentType}
                        floatingLabelText="Employment Type"
                        hintText="e.g. full-time, part-time, contract"
                        multiLine={true}
                      />
                      <br />
                      <FormsyText
                        name="workHours"
                        value={this.state.formData.workHours}
                        floatingLabelText="Working Hours"
                        hintText="e.g. 9am - 5pm, Mon - Fri"
                        multiLine={true}
                      />
                      <br />
                      <br />
                      {$jobDescriptionLabel}
                      <RichText
                        value={this.state.jobDescription}
                        onChange={this._onJobDescriptionChange}
                        onFocus={this._onJobDescriptionFocus}
                        onBlur={this._onJobDescriptionBlur}
                        className="text-editor"
                      />
                      <br />
                      { /*
                      <h3>Responsibilities</h3>
                      <RichText
                        value={this.state.responsibilities}
                        onChange={this._onResponsibilitiesChange}
                        className="text-editor"
                      />
                      <h3>Skills</h3>
                      <RichText
                        value={this.state.skills}
                        onChange={this._onSkillsChange}
                        className="text-editor"
                      />
                      <br />
                      <h3>Education Requirements</h3>
                      <RichText
                        value={this.state.educationRequirements}
                        onChange={this._onEducationRequirementsChange}
                        className="text-editor"
                      />
                      <br />
                      <h3>Experience Requirements</h3>
                      <RichText
                        value={this.state.experienceRequirements}
                        onChange={this._onExperienceRequirementsChange}
                        className="text-editor"
                      />
                      <br />
                      <h3>Qualifications</h3>
                      <RichText
                        value={this.state.qualifications}
                        onChange={this._onQualificationsChange}
                        className="text-editor"
                      />
                      <br />
                      <h3>Incentive Compensation</h3>
                      <RichText
                        value={this.state.incentiveCompensation}
                        onChange={this._onIncentiveCompensationChange}
                        className="text-editor"
                      />
                      <br />
                      <h3>Job Benefits</h3>
                      <RichText
                        value={this.state.jobBenefits}
                        onChange={this._onJobBenefitsChange}
                        className="text-editor"
                      />
                      <br />
                     */}
                    </Card>
                    <Card style={styles.cardStyle}>
                      <CardTitle
                        style={styles.cardTitleStyle}
                        title="How to apply"
                      >
                      </CardTitle>
                      <CardText style={styles.cardTextStyle}>
                        How do people apply for this job?
                        <br />
                        e.g. Email your resume to someone@example.com
                      </CardText>
                      <FormsyText
                        name="howToApply"
                        value={this.state.formData.howToApply}
                        required
                        floatingLabelText="How to apply? (required)"
                        fullWidth={true}
                        multiLine={true}
                      />
                      <br />
                    </Card>
                    <Card style={styles.cardStyle}>
                      <CardTitle
                        style={styles.cardTitleStyle}
                        title="About your company"
                      >
                      </CardTitle>
                      <CardText style={styles.cardTextStyle}>
                        Your company name and logo will be shown at the top in the job posting.
                        <br />
                        When the user clicks the company name or the logo (if provided), the user will be redirected to your company website (if provided).
                      </CardText>
                      <FormsyText
                        name="companyName"
                        value={this.state.formData.companyName}
                        required
                        floatingLabelText="Company name (required)"
                      />
                      <br />
                      <FormsyText
                        name="url"
                        value={this.state.formData.url}
                        validations="isUrl"
                        validationError={urlError}
                        hintText="https://www.example.com"
                        floatingLabelText="Your company website"
                        updateImmediately
                      />
                      <br />
                      <br />
                      <RaisedButton label="Upload your company logo" labelPosition="before">
                        <input type="file" style={styles.imageInput} onChange={this._handleImageChange} />
                      </RaisedButton>
                      {$imagePreview}
                      <br />
                      <br />
                    </Card>
                    <Card style={styles.cardStyle}>
                      <CardTitle
                        style={styles.cardTitleStyle}
                        title="Contact">
                      </CardTitle>
                      <CardText style={styles.cardTextStyle}>
                        <strong>Please make sure your email address is correct</strong>, as it will be used for important communication about your job posting.
                        <br />
                        <br />
                        Note: Your email address will <strong>NOT</strong> be displayed on the job posting.
                      </CardText>
                      <FormsyText
                        name="email"
                        value={this.state.formData.email}
                        required
                        validations="isEmail"
                        validationError={emailError}
                        floatingLabelText="Email address (required)"
                        hintText="someone@example.com"
                      />
                    </Card>
                    {this.renderStepActions(0, this.state.canSubmit)}
                  </Formsy.Form>
                </StepContent>
              </Step>
              <Step>
                <StepLabel>Preview</StepLabel>
                <StepContent style={styles.stepContentStyle}>
                  <form onSubmit={this._proceedToPaymentStep}>
                    <Card style={styles.cardStyle}>
                      <CardTitle
                        style={styles.cardTitleStyle}
                        title="Preview your job posting"
                      >
                      </CardTitle>
                      <RaisedButton
                        style={styles.submitStyle}
                        onClick={this._generatePreview}
                        label="Generate Preview Page"
                        disabled={this.state.isSubmittingForm}
                      />
                      <br />
                      <br />
                      { $linearProgress }
                      {$resultPage}
                    </Card>
                    {this.renderStepActions(1, true)}
                  </form>
                </StepContent>
              </Step>
              <Step>
                <StepLabel>Payment</StepLabel>
                <StepContent style={styles.stepContentStyle}>
                  <Card style={styles.cardStyle}>
                    <CardTitle
                      style={styles.cardTitleStyle}
                      title="Proceed to make payment with your credit card"
                    >
                    </CardTitle>
                    <CardText style={styles.cardTextStyle}>
                      <p>
                        <strong>Your job posting is $10.</strong>
                      </p>
                      <p>
                        We accept Visa, MasterCard, and American Express. Your payment is processed securely with Stripe.
                        <br />
                        <br />
                        <a href="https://stripe.com" target="_blank">
                          <img alt="powered by Stripe" src="./img/powered_by_stripe.svg" width="100px" />
                        </a>
                      </p>
                      <p>
                        Note: We reserve the right to remove any job posting deemed inappropriate or offensive (we will refund your money if we remove your job posting).
                      </p>
                    </CardText>
                    <CardReactFormContainer

                      // the id of the container element where you want to render the card element.  // the card component can be rendered anywhere (doesn't have to be in ReactCardFormContainer).
                      container="card-wrapper" // required

                      // an object contain the form inputs names.
                      // every input must have a unique name prop.
                      formInputsNames={
                        {
                          number: 'CCnumber', // optional — default "number"
                          expiry: 'CCexpiry',// optional — default "expiry"
                          cvc: 'CCcvc', // optional — default "cvc"
                          name: 'CCname' // optional - default "name"
                        }
                      }

                      // the class name attribute to add to the input field and the corresponding part of the card element,
                      // when the input is valid/invalid.
                      classes={
                        {
                          valid: 'valid-input', // optional — default 'jp-card-valid'
                          invalid: 'invalid-input' // optional — default 'jp-card-invalid'
                        }
                      }

                      // specify whether you want to format the form inputs or not
                      formatting={true} // optional - default true
                    >

                      <form onSubmit={this._completePayment}>
                        <TextField
                          id="CCname"
                          type="text"
                          data-stripe="name"
                          floatingLabelText="Full name"
                          hintText="Cardholder's name"
                          disabled={submittingPayment || isGeneratingJobPosting}
                        />
                        <br />
                        <TextField
                          id="CCnumber"
                          type="text"
                          data-stripe="number"
                          floatingLabelText="Card number"
                          disabled={submittingPayment || isGeneratingJobPosting}
                        />
                        <br />
                        <TextField
                          id="CCexpiry"
                          type="text"
                          data-stripe="exp"
                          hintText="MM/YY"
                          floatingLabelText="Expiry date"
                          disabled={submittingPayment || isGeneratingJobPosting}
                        />
                        <br />
                        <TextField
                          id="CCcvc"
                          type="text"
                          data-stripe="cvc"
                          hintText="3 or 4 digit code"
                          floatingLabelText="Security code (CVV)"
                          disabled={submittingPayment || isGeneratingJobPosting}
                        />
                        <br />
                        <br />
                        <div style={styles.cardWrapper} id="card-wrapper"></div>
                        <br />
                        <br />
                        {this.renderStepActions(2, true)}
                        {(submittingPayment || isGeneratingJobPosting) && (
                          <LinearProgress mode="indeterminate" />
                        )}
                        {paymentError && (
                          <p>Payment failed: {paymentError}</p>
                        )}
                      </form>
                    </CardReactFormContainer>
                  </Card>
                </StepContent>
              </Step>
            </Stepper>
            {finished && !jobPostingError && (
              <p style={{margin: '20px 14px'}}>
                Thanks for using our service!
                <br />
                <br />
                Here is the link to your job posting <a style={styles.linkStyle} href={this.state.generatedJobPostingUrl} target="_blank">{this.state.generatedJobPostingUrl}</a>
                <br />
                <br />
                You will also receive an email regarding the details of this job posting.
              </p>
            )}
            {finished && jobPostingError && (
              <p style={{margin: '20px 14px'}}>
                There is some issues with your job posting.
                <br />
                <br />
                We will look into the issue and get back to you as soon as possible.
              </p>
            )}
          </Paper>
          <footer style={styles.footerStyle}>
            <div>
              hallo.io
            </div>
          </footer>
        </div>
			</MuiThemeProvider>
    );
  }
}

export default App;
