import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import customTheme from './styles/customTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import CardReactFormContainer from 'card-react';
import { ReactScriptLoaderMixin } from 'react-script-loader';

import 'whatwg-fetch';

function responsiveWidth() {
  let width = window.innerWidth || document.body.clientWidth;
  if (width > 720) {
    return { width: '60%' };
  } else {
    return { width: '90%' };
  }
}

const styles = {
  imageInput: {
    cursor: 'pointer',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
  },
	paperStyle: {
		margin: 'auto',
		padding: 20,
	},
	submitStyle: {
		marginTop: 32,
	},
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
		};
  }
  render() {
    return (
			<MuiThemeProvider muiTheme={getMuiTheme(customTheme)}>
        <div>
          <CardReactFormContainer

            // the id of the container element where you want to render the card element.
            // the card component can be rendered anywhere (doesn't have to be in ReactCardFormContainer).
            container="card-wrapper" // required

            // an object contain the form inputs names.
            // every input must have a unique name prop.
            formInputsNames={
              {
                number: 'CCnumber', // optional — default "number"
                expiry: 'CCexpiry',// optional — default "expiry"
                cvc: 'CCcvc', // optional — default "cvc"
                name: 'CCname' // optional - default "name"
              }
            }

            // the class name attribute to add to the input field and the corresponding part of the card element,
            // when the input is valid/invalid.
            classes={
              {
                valid: 'valid-input', // optional — default 'jp-card-valid'
                invalid: 'invalid-input' // optional — default 'jp-card-invalid'
              }
            }

            // specify whether you want to format the form inputs or not
            formatting={true} // optional - default true
          >

            <form>
              <TextField type="text" data-stripe="name" id="CCname" floatingLabelText="Full name" />
              <br />
              <TextField type="text" data-stripe="number" id="CCnumber" floatingLabelText="Card number" />
              <br />
              <TextField type="text" data-stripe="exp" id="CCexpiry" hintText="MM/YY" floatingLabelText="Expiry date" />
              <br />
              <TextField type="text" data-stripe="cvc" id="CCcvc" floatingLabelText="CVC" />
              <br />
            </form>
          </CardReactFormContainer>
          <div id="card-wrapper"></div>
        </div>
			</MuiThemeProvider>
    );
  }
}

export default App;
