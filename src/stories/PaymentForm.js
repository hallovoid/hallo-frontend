var React = require('react');
var ReactScriptLoaderMixin = require('react-script-loader').ReactScriptLoaderMixin;
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import customTheme from '../styles/customTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import CardReactFormContainer from 'card-react';

var PaymentForm = React.createClass({
  mixins: [ ReactScriptLoaderMixin ],

  getInitialState: function() {
    return {
      stripeLoading: true,
      stripeLoadingError: false,
      submitDisabled: false,
      paymentError: null,
      paymentComplete: false,
      token: null
    };
  },

  getScriptURL: function() {
    return 'https://js.stripe.com/v2/';
  },

  onScriptLoaded: function() {
    if (!PaymentForm.getStripeToken) {
      // Put your publishable key here
      Stripe.setPublishableKey('pk_test_NaIHlmi8Jti0px06lz0AAeRy');

      this.setState({ stripeLoading: false, stripeLoadingError: false });
    }
  },

  onScriptError: function() {
    this.setState({ stripeLoading: false, stripeLoadingError: true });
  },

  onSubmit: function(event) {
    var self = this;
    event.preventDefault();
    this.setState({ submitDisabled: true, paymentError: null });
    // send form here
    Stripe.createToken(event.target, function(status, response) {
      if (response.error) {
        self.setState({ paymentError: response.error.message, submitDisabled: false });
      }
      else {
        self.setState({ paymentComplete: true, submitDisabled: false, token: response.id });
        // make request to your server here!
      }
    });
  },

  render: function() {
    if (this.state.stripeLoading) {
      return <div>Loading</div>;
    }
    else if (this.state.stripeLoadingError) {
      return <div>Error</div>;
    }
    else if (this.state.paymentComplete) {
      return <div>Payment Complete!</div>;
    }
    else {
      return (
			<MuiThemeProvider muiTheme={getMuiTheme(customTheme)}>
        <div>
          <CardReactFormContainer

            // the id of the container element where you want to render the card element.
            // the card component can be rendered anywhere (doesn't have to be in ReactCardFormContainer).
            container="card-wrapper" // required

            // an object contain the form inputs names.
            // every input must have a unique name prop.
            formInputsNames={
              {
                number: 'CCnumber', // optional — default "number"
                expiry: 'CCexpiry',// optional — default "expiry"
                cvc: 'CCcvc', // optional — default "cvc"
                name: 'CCname' // optional - default "name"
              }
            }

            // the class name attribute to add to the input field and the corresponding part of the card element,
            // when the input is valid/invalid.
            classes={
              {
                valid: 'valid-input', // optional — default 'jp-card-valid'
                invalid: 'invalid-input' // optional — default 'jp-card-invalid'
              }
            }

            // specify whether you want to format the form inputs or not
            formatting={true} // optional - default true
          >

            <form onSubmit={this.onSubmit} >
              <span>{ this.state.paymentError }</span><br />
              <TextField type="text" data-stripe="name" id="CCname" floatingLabelText="Full name" />
              <br />
              <TextField type="text" data-stripe="number" id="CCnumber" floatingLabelText="Card number" />
              <br />
              <TextField type="text" data-stripe="exp" id="CCexpiry" hintText="MM/YY" floatingLabelText="Expiry date" />
              <br />
              <TextField type="text" data-stripe="cvc" id="CCcvc" floatingLabelText="CVC" />
              <br />
              <br />
              <input disabled={false} type='submit' value='Purchase' />
            </form>
          </CardReactFormContainer>
          <div id="card-wrapper"></div>
        </div>
			</MuiThemeProvider>
      );
    }
  }
});

module.exports = PaymentForm;
