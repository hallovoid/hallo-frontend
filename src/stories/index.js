import React from 'react';
import { storiesOf, action, linkTo } from '@kadira/storybook';
import Button from './Button';
import Welcome from './Welcome';
import PaymentForm from './PaymentForm';
import StripePaymentForm from '../StripePaymentForm';

import '../index.css';
import '../card.css';
import App from '../App';

storiesOf('App', module)
  .add('default view', () => (
    <App />
  ))

storiesOf('Welcome', module)
  .add('to Storybook', () => (
    <Welcome showApp={linkTo('Button')}/>
  ));

storiesOf('Button', module)
  .add('with text', () => (
    <Button onClick={action('clicked')}>Hello Button</Button>
  ))
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>😀 😎 👍 💯</Button>
  ));

storiesOf('StripePaymentForm', module)
  .add('index', () => (
    <StripePaymentForm />
  ));

storiesOf('PaymentForm', module)
  .add('index', () => (
    <PaymentForm />
  ));
