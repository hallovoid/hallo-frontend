import React, {Component} from 'react';
import RichTextEditor from 'react-rte';

class RichText extends Component {
  render() {
		// The toolbarConfig object allows you to specify custom buttons, reorder buttons and to add custom css classes.
		// Supported inline styles: https://github.com/facebook/draft-js/blob/master/docs/Advanced-Topics-Inline-Styles.md
		// Supported block types: https://github.com/facebook/draft-js/blob/master/docs/Advanced-Topics-Custom-Block-Render.md#draft-default-block-render-map
		const toolbarConfig = {
			// Optionally specify the groups to display (displayed in the order listed).
			display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'HISTORY_BUTTONS'],
			INLINE_STYLE_BUTTONS: [
				{label: 'Bold', style: 'BOLD', className: 'custom-css-class'},
				{label: 'Italic', style: 'ITALIC'}
			],
			BLOCK_TYPE_BUTTONS: [
				{label: 'UL', style: 'unordered-list-item'},
				{label: 'OL', style: 'ordered-list-item'}
			]
		};
		return (
			<RichTextEditor {...this.props} toolbarConfig={toolbarConfig} />
		);
	}
}

export default RichText
